document.addEventListener("DOMContentLoaded", () => {
    const modal = document.getElementById("modal") as HTMLElement;
    const closeBtn = document.getElementsByClassName("close")[0] as HTMLElement;
    const body = document.querySelector('body') as HTMLBodyElement;

    document.querySelectorAll('.learn-more').forEach(button => {
        button.addEventListener('click', () => {
            modal.style.display = "block";
            body.style.overflow = "hidden"
        });
    });

    closeBtn.addEventListener('click', () => {
        modal.style.display = "none";
        body.style.overflow = "unset"
    });
});
