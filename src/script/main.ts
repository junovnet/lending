function setupArrowButton(): void {

    let targets: NodeListOf<Element> = document.querySelectorAll("header, .banner");

    let btnArrowUp: HTMLElement | null = document.getElementById('btn-arrow-up');
    let rootElement: HTMLElement | null = document.documentElement;

    if (!btnArrowUp || targets.length === 0 || !rootElement) return;

    function callback(entries: IntersectionObserverEntry[]): void {
        entries.forEach((entry: IntersectionObserverEntry) => {
            if (entry.isIntersecting) {
                if (btnArrowUp) btnArrowUp.style.display = "none";
            } else {
                if (btnArrowUp) btnArrowUp.style.display = "block";
            }
        });
    }

    function scrollToTop(): void {
        if (rootElement) {
            rootElement.scrollTo({
                top: 0,
                behavior: "smooth"
            });
        }
    }

    if (btnArrowUp) btnArrowUp.addEventListener("click", scrollToTop);

    let observer: IntersectionObserver = new IntersectionObserver(callback);

    targets.forEach(target => observer.observe(target));
}

setupArrowButton();

function accordionToggleButton() {
    const accordionHeaders = document.querySelectorAll<HTMLElement>('.accordion-header');

    accordionHeaders.forEach(header => {
        header.addEventListener('click', (event: MouseEvent) => {
            const accordionItem = header.parentElement;
            const accordionContent = accordionItem?.querySelector<HTMLDivElement>('.accordion-content');
            const accordionToggle = header.querySelector<HTMLButtonElement>('.accordion-toggle');

            if (!accordionContent || !accordionToggle) return;

            const isOpen = accordionContent.style.maxHeight && accordionContent.style.maxHeight !== '0px';

            if (isOpen) {
                accordionContent.style.maxHeight = '0px';
                accordionContent.style.padding = '0 0';
                accordionToggle.classList.remove('minus');
                accordionToggle.classList.add('plus');
                header.classList.remove('open');
            } else {
                accordionContent.style.maxHeight = accordionContent.scrollHeight + 'px';
                accordionContent.style.padding = '20px 0';
                accordionToggle.classList.remove('plus');
                accordionToggle.classList.add('minus');
                header.classList.add('open');
            }

            event.stopPropagation();
        });
    });
}

accordionToggleButton();

const burgerMenu = document.getElementById('burger-menu') as HTMLElement;
const body = document.querySelector('body') as HTMLBodyElement;
const overlay = document.getElementById('menu') as HTMLElement;

burgerMenu.addEventListener('click',function(){
  this.classList.toggle("close-menu");
  body.classList.toggle('menu-opened');
  overlay.classList.toggle("overlay");
});
