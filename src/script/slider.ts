const loadMoreBtn = document.querySelector('#btn-more') as HTMLButtonElement;
const cards = Array.from(document.querySelectorAll('.card') as NodeListOf<HTMLElement>);
let currentItem = 3;

const updateButtonVisibility = () => {
    loadMoreBtn.style.display = (cards.length > currentItem) ? "block" : "none";
};

const loadMoreCards = () => {
    cards.slice(currentItem, currentItem + 3).forEach(card => {
        card.style.display = "block";
    });
    currentItem += 3;
    updateButtonVisibility();
};

cards.slice(0, currentItem).forEach(card => {
    card.style.display = "block";
});
updateButtonVisibility();

loadMoreBtn.addEventListener('click', loadMoreCards);